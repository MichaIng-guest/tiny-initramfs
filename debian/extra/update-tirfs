#!/bin/sh

set -e
umask 0022

usage() {
  echo "Usage: $0 [-k version]"
  echo ""
  echo "Updates system tiny-initramfs images"
  echo ""
  echo "Options:"
  echo "    -k, --kernel-version=VERSION  The kernel version for which the initramfs"
  echo "                                  image is to be updated. (default: all)"
}

if ! OPTIONS=$(getopt -o k:h -l kernel-version:,help -n "$0" -- "$@") ; then
  usage >&2
  exit 1
fi
eval set -- "$OPTIONS"

VERSION="all"
while true; do
  case "$1" in
    -h|--help)
      usage
      exit 0
      ;;
    -k|--kernel-version)
      VERSION="$2"
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "$0: internal error" >&2
      exit 1
      ;;
  esac
done

if [ $# -gt 0 ] ; then
  echo "$0: too many options" >&2
  usage >&2
  exit 1
fi

# Called from a maintainer script -> resort to triggers instead
if [ -n "$DPKG_MAINTSCRIPT_PACKAGE" ]; then
  echo "$0: WARNING: called from dpkg maintscript, please use triggers instead." >&2
  if dpkg-trigger --no-await update-initramfs; then
    echo "update-tirfs: deferring update (trigger activated)"
    exit 0
  fi
fi

if [ x"$VERSION" = x"all" ] ; then
  for V in /boot/vmlinu[xz]-* ; do
    if ! [ -r "${V}" ] ; then continue ; fi
    V=${V#/boot/vmlinu?-}
    mktirfs -o /boot/initrd.img-"${V}" "${V}"
  done
else
  if ! ls /boot/vmlinu[xz]-"${VERSION}" >/dev/null 2>&1 ; then
    echo "$0: kernel version ${VERSION} does not appear to be installed." >&2
    exit 1
  fi
  mktirfs -o /boot/initrd.img-"${VERSION}" "${VERSION}"
fi
