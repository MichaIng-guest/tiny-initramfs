.TH MKTIRFS 8 "Jan 2016" "" "tiny\-initramfs Manual"
.SH NAME
mktirfs \- tool for creating a tiny\-initramfs image
.SH SYNOPSIS
\fBmktirfs\fR \-o outputfile [options] [kernel-version]

.SH "DESCRIPTION"
The
.B mktirfs 
script generates an initramfs image that contains the tiny\-initramfs
.I init
binary. Invoking
.B mktirfs
is meant for expert users that want to have greater control over their
initramfs image. Typically it would be called from
.BR update\-tirfs .

Images created with this utility are designed to be very small and
fast, and thus ideal for use cases where the amount of space available
in the boot loader is very low. If no modules and no microcode updates
are included in the image, it will be less than 16 kiB in size. This
comes at the cost of runtime flexibility and supported features of the
generated images. It is not meant to replace traditional initramfs
implementations (such as \fIdracut\fR or \fIinitramfs-tools\fR) in the
generic case, but provide a lean alternative for common use cases.

Please read
.I /usr/share/doc/tiny\-initramfs\-core/README.md
for an overview of the use cases it was designed for and the features
it supports.

.SH OPTIONS

.TP
\fB\-o\fR, \fB\-\-output=\fIimagefile\fP
Where to store the resulting initramfs image. This option must be
specified.

.TP
\fB\-m\fR, \fB\-\-auto\-modules=\fIyes/no\fP
Whether to try to automatically detect which modules are required to
mount the / and /usr file systems and include them in the resulting
initramfs image.

Defaults to \fIyes\fR.

.TP
\fB\-\-include\-modules=\fImod1,mod2,...\fP
Which modules to directly add to the initramfs image, regardless of
the setting for \fB-m\fR. Dependencies of the modules will be
resolved by \fBmktirfs\fR. This option is useful if the required
modules are not properly detected or one wants to create an image for
a different computer.

By default this list is empty.

.TP
\fB\-M\fR, \fB\-\-microcode=\fIgeneric/yes/no\fP
Whether to prepend CPU microcode updates (if installed) to the
initramfs image. Microcode updates may be prepended to the initramfs,
and the kernel will automatically load them at boot time. (This
requires the
.B CONFIG_MICROCODE_EARLY
kernel option in conjunction with either
.B CONFIG_MICROCODE_INTEL
or
.B CONFIG_MICROCODE_AMD
compiled into the kernel.) The microcode updates can be found in the
(non-free) packages
.I intel-microcode
and
.I amd64-microcode
packages.

If the \fIgeneric\fR setting is specified, all installed microcode
updates will be added to the initramfs image; if \fIyes\fR is specified
only the update for the processor on which \fBmktirfs\fR was run on will
be included.

Microcode updates not being installed (or no microcode updates for the
specific processor being available) is not considered to be an error
and \fBmktirfs\fR will proceed regardless.

Defaults to \fIyes\fR. Is only supported on x86 processors and will be
ignored otherwise.

.TP
\fB\-d\fR, \fB\-\-debug=\fIyes/no\fP
If set to \fIyes\fR, this option will install a (larger) version of the
\fIinit\fR binary into the initramfs image that prints additional debug
messages while booting, and waits 5 seconds after mounting the root
file system before handing control over to the system's \fIinit\fR
process. This allows the administrator to debug boot problems with
initramfs images.

Defaults to \fIno\fR.

.TP
\fIkernel-version\fP
The kernel version for which to generate the initramfs image. This is
only relevant if modules are to be added (either automatically or
manually) to the initramfs image. If no modules are included in the
initramfs image, the resulting image may be used with any kernel that
has the drivers required to mount the root filesystem built in.

By default the version returned by
.I uname \-r
is used.

.SH CONFIGURATION

Except for \fB-o\fR and the kernel version, defaults for all the
options may be specified in a configuration file
.IR /etc/tiny\-initramfs/tiny\-initramfs.conf .
This file does not exist by default, but an example version may be
found in
.IR /usr/share/doc/tiny\-initramfs\-core/tiny\-initramfs.conf.example .

Any settings that
.B update\-tirfs
is to use when calling
.B mktirfs
should be set in that configuration file.

.SH HOOKS

There are no hooks within the initramfs itself, the \fIinit\fR binary
is a very small statically linked C program.

.nr step 1 1
However \fBmktirfs\fR supports hooks when it comes to creating the
initramfs image. The overall logic of \fBmktirfs\fR is as follows:
.IP \n[step]. 4
Determine the list of kernel module names (without dependencies) to
be added to the initramfs.
.IP \n+[step].
Run \fIstart\fR hooks.
.IP \n+[step].
Copy \fIinit\fR executable, add directories, copy microcode.
.IP \n+[step].
Run \fImiddle\fR hooks.
.IP \n+[step].
Determine dependencies of modules, add all module files.
.IP \n+[step].
Create initramfs image.
.IP \n+[step].
Run \fIend\fR hooks.
.IP \n+[step].
Copy image to its final location.
.PP

\fBmktirfs\fR looks for hooks in the
.I /usr/share/tiny\-initramfs/hooks.type
and
.I /etc/tiny\-initramfs/hooks.type
directories, e.g.
.IR /etc/tiny\-initramfs/hooks.start .
The directory in \fI/usr/share\fR is the domain of other Debian
packages, while the directory in \fI/etc\fR is designated for the
administrator of the system. Hooks will be called in alphabetical
order, and hooks in \fI/etc\fR will override hooks in
\fI/usr/share\fR.
.BR run-parts (8)
is used to determine what hooks should be considered.

Hooks are \fIsourced\fR, not executed, so calling \fBexit\fR in a hook
aborts the creation of the initramfs image. It also allows the hooks to
modify variables that will be interpreted later.

Note that \fBmktirfs\fR uses \fI/bin/sh\fR as the interpreter, which is
.BR dash (1)
by default on Debian systems, so not all
.BR bash (1)
functionality is available within hooks.

The following shell variables are available from within hooksL
.IP VERSION 0.4i
The kernel version for which the initramfs is built.
.IP DEBUG
The value of the \fB\-d\fR command line option.
.IP AUTO_MODULES
The value of the \fB\-m\fR command line option.
.IP INCLUDE_MODULES
The value of the \fB\-\-include\-modules\fR command line option.
.IP MICROCODE
The value of the \fB\-M\fR command line option.
.IP OUTPUT
The value of the \fB\-o\fR command line option. This should not be
used, as \fBmktirfs\fR should only replace it if image creation was
successful.
.IP image_name
The temporary file name of the initramfs image that is to be created.
Will be empty except for \fIend\fR hooks.
.IP initramfs_dir
The temporary directory that contains the contents of the initramfs
image. Any directories and files placed here will be part of the final
initramfs image.
.IP early_dir
The temporary directory that contains the contents of the early part of
the initramfs image. This is used for storing microcode updates for the
CPU, but the kernel also supports other updates here, e.g. updates to
ACPI tables. If this directory is non-empty after the \fImiddle\fR
hooks an uncompressed CPIO image of this directory will be prepended to
the final initramfs image by \fBmktirfs\fR. If it is empty (no
microcode, no other early updates), the initramfs image will just
contain the compressed CPIO archive of the \fIinitramfs_dir\fR
directory.
.IP modules_list
The name of the temporary file that contains the list of module names
that will be added to the initramfs image (one line per module name).
After the \fImiddle\fR hooks all modules, together with their
dependencies, will be added to the initramfs image, any module options
defined in \fI/etc/modprobe.d\fR for those modules will also be
included.
.PP

.SH EXAMPLE

.nf
Create a new initramfs image for the current kernel:

.ft R
        mktirfs -o /boot/initrd.img.tiny\-$(uname \-r)
.nf

.nf
Create a new initramfs image for a specific kernel:

.ft R
        mktirfs -o /boot/initrd.img.tiny\-4.2.0 4.2.0
.nf

.SH FILES
.TP
/etc/tiny\-initramfs/tiny\-initramfs.conf
If present, values in there will be used as defaults for the command
line arguments of
.BR mktirfs .
.TP
/usr/share/doc/tiny\-initramfs\-core/tiny\-initramfs.conf.example
An example configuration file that may be copied to the above location.
.TP
/usr/share/tiny\-initramfs/hooks.start
.TP
/usr/share/tiny\-initramfs/hooks.middle
.TP
/usr/share/tiny\-initramfs/hooks.end
If present, hooks created by other Debian packages will be sourced from
these directories.
.TP
/etc/tiny\-initramfs/hooks.start
.TP
/etc/tiny\-initramfs/hooks.middle
.TP
/etc/tiny\-initramfs/hooks.end
If present, hooks created by the system administrator will be sourced
from thsee directories. Hooks will replace those with the same name as
hooks in \fI/usr/share\fR.

.SH "SEE ALSO"
.BR update\-tirfs (8)

.SH AUTHORS
Christian Seiler <christian@iwakd.de>
