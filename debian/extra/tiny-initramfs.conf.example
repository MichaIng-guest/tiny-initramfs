# tiny-initramfs.conf
# Any changes to this file require an invocation of update-tirfs

#
# Whether to use the debug versions of tiny-initramfs.
#
# In order to debug boot problems that may arise with tiny-initramfs,
# there is variant that is compiled with some additional verbosity
# enabled. Also, it will wait 5s just before invoking init(1) to allow
# administrators to properly read all messages before the system boots.
# Since debug messages increase the size noticeably, this option is not
# included in the regular variant.
#
# Possible values are 'yes' or 'no', default is 'no'.
#
DEBUG=no

#
# Whether to include early processor microcode updates in the initramfs
# image (x86 platforms only). Possible values are:
#
#   - no
#        Don't include early processor microcode updates
#   - yes
#        Include processor-specific microcode updates if the package is
#        installed
#   - generic
#        Include processor-specific microcode updates for all installed
#        microcode packages (regardless of host processor)
#
# The default is 'yes'.
#
# This option is ignored on non-x86 platforms.
#
MICROCODE=yes

#
# Whether to auto-include modules in the initramfs image.
#
# This determines whether tiny-initramfs should try to auto-detect
# the modules necessary to mount the root and /usr file systems and
# add them to the initramfs image.
#
# If the means required to mount / and /usr are not compiled into the
# kernel, including the modules are required for the system to boot.
#
# If you are using a kernel that has support for mounting the root and
# /usr file systems built-in, setting this to 'no' can skip this skep
# and potentially avoid false positives.
#
# Note that any modules added by hooks are always included in the
# initramfs image.
#
# The default is 'yes'.
#
AUTO_MODULES=yes

#
# Which modules to include manually in the initramfs image.
#
# A comma-separated list of modules that, in addition to their
# dependencies, should always be added to initramfs image.
#
# See the comments for the AUTO_MODULES setting for details on module
# loading from tiny-initramfs images.
#
INCLUDE_MODULES=""
